function varargout = OptimGui(varargin)
% OPTIMGUI MATLAB code for OptimGui.fig
%      OPTIMGUI, by itself, creates a new OPTIMGUI or raises the existing
%      singleton*.
%
%      H = OPTIMGUI returns the handle to a new OPTIMGUI or the handle to
%      the existing singleton*.
%
%      OPTIMGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in OPTIMGUI.M with the given input arguments.
%
%      OPTIMGUI('Property','Value',...) creates a new OPTIMGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before OptimGui_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to OptimGui_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help OptimGui

% Last Modified by GUIDE v2.5 26-Nov-2019 18:31:46

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @OptimGui_OpeningFcn, ...
                   'gui_OutputFcn',  @OptimGui_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before OptimGui is made visible.
function OptimGui_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to OptimGui (see VARARGIN)

% Choose default command line output for OptimGui
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes OptimGui wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = OptimGui_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in inputFileButton.
function inputFileButton_Callback(hObject, eventdata, handles)
% hObject    handle to inputFileButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% [file_name, path_name]  = uigetfile('*.xlsx');
% file_path               = [path_name file_name];
file_path               = 'optimization_gui/Data/ARN083_t.xlsx';
data_table              = readtable(file_path);

V                       = data_table{:,1};
DS                      = data_table{:,2}; 
numOfV                  = 1:numel(data_table{:,1});
numOfDS                 = 1:numel(data_table{:,2});

axes(handles.axes1);
plot(numOfV,V);
set(handles.uitable1, 'Data', [V,DS]);

axes(handles.axes2);
ylabel('Trials', 'FontSize', 12);
plot(numOfDS,DS);
% set(handles.uitable1, 'Data', [V,DS]);


% --- Executes on button press in graphButton.
function graphButton_Callback(hObject, eventdata, handles)
% hObject    handle to graphButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

data = get(handles.uitable1, 'Data');
col1 = data(:,1);
col2 = data(:,2);
plot(col1, col2);

% --- Executes when entered data in editable cell(s) in uitable1.
function uitable1_CellEditCallback(hObject, eventdata, handles)
% hObject    handle to uitable1 (see GCBO)
% eventdata  structure with the following fields (see MATLAB.UI.CONTROL.TABLE)
%	Indices: row and column indices of the cell(s) edited
%	PreviousData: previous data for the cell(s) edited
%	EditData: string(s) entered by the user
%	NewData: EditData or its converted form set on the Data property. Empty if Data was not changed
%	Error: error string when failed to convert EditData to appropriate value for Data
% handles    structure with handles and user data (see GUIDATA)


% --- Executes when selected cell(s) is changed in uitable1.
function uitable1_CellSelectionCallback(hObject, eventdata, handles)
% hObject    handle to uitable1 (see GCBO)
% eventdata  structure with the following fields (see MATLAB.UI.CONTROL.TABLE)
%	Indices: row and column indices of the cell(s) currently selecteds
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in saveButton.
function saveButton_Callback(hObject, eventdata, handles)
% hObject    handle to saveButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
data = get(handles.uitable1, 'Data');
col1 = data(:,1);
col2 = data(:,2);
M = [col1 col2];
csvwrite('savedFile.csv', M);


% --- Executes on button press in optimizeButton4.
function optimizeButton4_Callback(hObject, eventdata, handles)
% hObject    handle to optimizeButton4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


data            = get(handles.uitable1, 'Data');
X_v             = data(:,1);
Y_ds            = data(:,2);

lower_bound_input = str2double(get(handles.lowerBoundButton, 'String'));
upper_bound_input = str2double(get(handles.upperBoundButton, 'String'));
beta_input = str2double(get(handles.betaButton, 'String'));
eta_input = str2double(get(handles.etaButton, 'String'));
alpha_input = str2double(get(handles.alphaButton, 'String'));
use_hyperprior_input = get(handles.useHyperpriorCheck,'Value');
use_safe_opt_input = get(handles.useSafe, 'Value');

safe_seed       = [0 .5 1];
lower_bound     = lower_bound_input;
upper_bound     = upper_bound_input;
threshold       = 0;

USE_HYPERPRIOR  = use_hyperprior_input;
USE_SAFE_OPT    = use_safe_opt_input;
PLOT            = 0;

BETA            = beta_input;
ETA             = eta_input;
ALPHA           = alpha_input;

nan_idx         = isnan(Y_ds);

X_v             = X_v(~nan_idx);
Y_ds            = Y_ds(~nan_idx);



[x_sample, x_opt_est, y_opt_est, S, ucb] = safe_opt_update_memory(...
    X_v, Y_ds, BETA, ETA, ALPHA, safe_seed, threshold, lower_bound,...
    upper_bound, USE_HYPERPRIOR, USE_SAFE_OPT, PLOT);


function lowerBoundButton_Callback(hObject, eventdata, handles)
% hObject    handle to lowerBoundButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of lowerBoundButton as text
%        str2double(get(hObject,'String')) returns contents of lowerBoundButton as a double


% --- Executes during object creation, after setting all properties.
function lowerBoundButton_CreateFcn(hObject, eventdata, handles)
% hObject    handle to lowerBoundButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function upperBoundButton_Callback(hObject, eventdata, handles)
% hObject    handle to upperBoundButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of upperBoundButton as text
%        str2double(get(hObject,'String')) returns contents of upperBoundButton as a double


% --- Executes during object creation, after setting all properties.
function upperBoundButton_CreateFcn(hObject, eventdata, handles)
% hObject    handle to upperBoundButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function betaButton_Callback(hObject, eventdata, handles)
% hObject    handle to betaButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of betaButton as text
%        str2double(get(hObject,'String')) returns contents of betaButton as a double


% --- Executes during object creation, after setting all properties.
function betaButton_CreateFcn(hObject, eventdata, handles)
% hObject    handle to betaButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function etaButton_Callback(hObject, eventdata, handles)
% hObject    handle to etaButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of etaButton as text
%        str2double(get(hObject,'String')) returns contents of etaButton as a double


% --- Executes during object creation, after setting all properties.
function etaButton_CreateFcn(hObject, eventdata, handles)
% hObject    handle to etaButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function alphaButton_Callback(hObject, eventdata, handles)
% hObject    handle to alphaButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of alphaButton as text
%        str2double(get(hObject,'String')) returns contents of alphaButton as a double


% --- Executes during object creation, after setting all properties.
function alphaButton_CreateFcn(hObject, eventdata, handles)
% hObject    handle to alphaButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function safe_seedButton_Callback(hObject, eventdata, handles)
% hObject    handle to safe_seedButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of safe_seedButton as text
%        str2double(get(hObject,'String')) returns contents of safe_seedButton as a double



% --- Executes during object creation, after setting all properties.
function safe_seedButton_CreateFcn(hObject, eventdata, handles)
% hObject    handle to safe_seedButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in useHyperpriorCheck.
function useHyperpriorCheck_Callback(hObject, eventdata, handles)
% hObject    handle to useHyperpriorCheck (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of useHyperpriorCheck


% --- Executes on button press in useSafe.
function useSafe_Callback(hObject, eventdata, handles)
% hObject    handle to useSafe (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of useSafe
