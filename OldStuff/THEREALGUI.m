function varargout = THEREALGUI(varargin)
% THEREALGUI MATLAB code for THEREALGUI.fig
%      THEREALGUI, by itself, creates a new THEREALGUI or raises the existing
%      singleton*.
%
%      H = THEREALGUI returns the handle to a new THEREALGUI or the handle to
%      the existing singleton*.
%
%      THEREALGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in THEREALGUI.M with the given input arguments.
%
%      THEREALGUI('Property','Value',...) creates a new THEREALGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before THEREALGUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to THEREALGUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help THEREALGUI

% Last Modified by GUIDE v2.5 21-Dec-2018 12:19:47

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @THEREALGUI_OpeningFcn, ...
                   'gui_OutputFcn',  @THEREALGUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT
% 

% --- Executes just before THEREALGUI is made visible.
function THEREALGUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to THEREALGUI (see VARARGIN)

% Choose default command line output for THEREALGUI
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes THEREALGUI wait for user response (see UIRESUME)
% uiwait(handles.figure1);
set(handles.uitable1,'Visible','Off')
set(handles.uipanel6,'Visible','Off')
set(handles.Graphpushbutton1,'Visible','Off')


% --- Outputs from this function are returned to the command line.
function varargout = THEREALGUI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes during object creation, after setting all properties.
function axes1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to axes1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called




% --- Executes on button press in Graphpushbutton1.
function Graphpushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to Graphpushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

dataa= getappdata(handles.uipanel3,'dataset')
a=dataa(:,1)
dataa(:,2)=0
dataa(:,3)=0
d=dataa(:,4)
y=dataa(:,end)
x=sum(~all(dataa==0))-1

axes(handles.uipanelwaxes)
subplot(5,1,1)
plot(a,y)
subplot(5,1,2)
plot(a,y)
subplot(5,1,3)
plot(a,y)
subplot(5,1,4)
plot(a,y)
subplot(5,1,5)
plot(a,y)


min=str2double(getappdata(handles.uipanel3,'min'))
max=str2double(getappdata(handles.uipanel3,'max'))
if ~isnan(min) && ~isnan(max)
    xlim([min max])
end




function InputForParameters_Callback(hObject, eventdata, handles)
% hObject    handle to InputForParameters (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of InputForParameters as text
%        str2double(get(hObject,'String')) returns contents of InputForParameters as a double

para=get(handles.InputForParameters,'String')
setappdata(handles.uipanel3,'PP',para)


% --- Executes during object creation, after setting all properties.
function InputForParameters_CreateFcn(hObject, eventdata, handles)
% hObject    handle to InputForParameters (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in GenerateBoundsPushButton.
function GenerateBoundsPushButton_Callback(hObject, eventdata, handles)
% hObject    handle to GenerateBoundsPushButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

n=getappdata(handles.uipanel3,'PP')
s=getappdata(handles.uipanel3, 'PP')
x=str2double(s)
d=get(handles.uitable1, 'data');
a=0
b=10
%data = zeros(100,x+1)

set(handles.uitable1,'Visible','Off')
set(handles.uipanel6,'Visible','On')
set(handles.Graphpushbutton1,'Visible','Off')
set(handles.popupmenu4,'String','Double-Click To Select');
% --- Executes when entered data in editable cell(s) in uitable1.

function uitable1_CellEditCallback(hObject, eventdata, handles)
% hObject    handle to uitable1 (see GCBO)
% eventdata  structure with the following fields (see MATLAB.UI.CONTROL.TABLE)
%	Indices: row and column indices of the cell(s) edited
%	PreviousData: previous data for the cell(s) edited
%	EditData: string(s) entered by the user
%	NewData: EditData or its converted form set on the Data property. Empty if Data was not changed
%	Error: error string when failed to convert EditData to appropriate value for Data
% handles    structure with handles and user data (see GUIDATA)

data=get(handles.uitable1,'Data')
setappdata(handles.uipanel3, 'dataset', data)
set(handles.uitable1,'ColumnEditable',true)
set(handles.uitable1,'ColumnName','favor')

if get(handles.popupmenu4,'String')==1
    lowlim=get(hObject,'String')
    setappdata(handles.uipanel3,'min',lowlim)
    dataa(:,1)<min
end

% --- Executes during object creation, after setting all properties.
function uipanelwaxes_CreateFcn(hObject, eventdata, handles)
% hObject    handle to uipanelwaxes (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called



function LowLim_Callback(hObject, eventdata, handles)
% hObject    handle to LowLim (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of LowLim as text
%        str2double(get(hObject,'String')) returns contents of LowLim as a double

lowlim=get(handles.LowLim,'String')
setappdata(handles.uipanel3,'minlim',lowlim)


% --- Executes during object creation, after setting all properties.
function LowLim_CreateFcn(hObject, eventdata, handles)
% hObject    handle to LowLim (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function UpLim_Callback(hObject, eventdata, handles)
% hObject    handle to UpLim (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of UpLim as text
%        str2double(get(hObject,'String')) returns contents of UpLim as a double
%uplim=get(handles.UpLim,'String');
%setappdata(handles.uipanel3,'maxlim',uplim);



Val=getappdata(handles.uipanel3,'PopupValue');
LastVal=getappdata(handles.uipanel3,'LastPopupValue');

uplim = string(get(handles.UpLim,'String'));
setappdata(handles.uipanel3, 'Old', uplim)


if Val == '1'
    u = [uplim]
    setappdata(handles.uipanel3, 'old',uplim)
else
    u = [getappdata(handles.uipanel3,'old') uplim]
end



% --- Executes during object creation, after setting all properties.
function UpLim_CreateFcn(hObject, eventdata, handles)
% hObject    handle to UpLim (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



% --- Executes on selection change in popupmenu4.
function popupmenu4_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu4 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu4

e=str2num(getappdata(handles.uipanel3,'PP'));
f=num2str(1:e);
f=f(~isspace(f));
set(handles.popupmenu4,'String',f');

contents = cellstr(get(hObject,'String'));
LastVal=contents(end);
Val=contents{get(hObject,'Value')};

setappdata(handles.uipanel3,'PopupValue',Val)
setappdata(handles.uipanel3,'LastPopupValue',LastVal)



% --- Executes during object creation, after setting all properties.
function popupmenu4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on button press in CreateTablePushButton.
function CreateTablePushButton_Callback(hObject, eventdata, handles)
% hObject    handle to CreateTablePushButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

n=getappdata(handles.uipanel3,'PP');
s=getappdata(handles.uipanel3, 'PP');
x=str2double(s);
d = get(handles.uitable1, 'data');

e=str2num(getappdata(handles.uipanel3,'PP'));
min_limit=getappdata(handles.uipanel3,'minlim');
max_limit=getappdata(handles.uipanel3,'maxlim');
a=str2double(min_limit);
b=str2double(max_limit);
data=((b-a).*rand(100,x+1)+a);
setappdata(handles.uipanel3,'dataset',data)

%data = zeros(100,x+1)





set(handles.uitable1,'Data',data)
set(handles.uitable1,'Visible','On')
set(handles.uipanel6,'Visible','On')
set(handles.Graphpushbutton1,'Visible','On')
