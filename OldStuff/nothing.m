function varargout = THEREALGUI(varargin)
% THEREALGUI MATLAB code for THEREALGUI.fig
%      THEREALGUI, by itself, creates a new THEREALGUI or raises the existing
%      singleton*.
%
%      H = THEREALGUI returns the handle to a new THEREALGUI or the handle to
%      the existing singleton*.
%
%      THEREALGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in THEREALGUI.M with the given input arguments.
%
%      THEREALGUI('Property','Value',...) creates a new THEREALGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before THEREALGUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to THEREALGUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help THEREALGUI

% Last Modified by GUIDE v2.5 18-Oct-2018 16:40:15

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @THEREALGUI_OpeningFcn, ...
                   'gui_OutputFcn',  @THEREALGUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT
% 

% --- Executes just before THEREALGUI is made visible.
function THEREALGUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to THEREALGUI (see VARARGIN)

% Choose default command line output for THEREALGUI
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes THEREALGUI wait for user response (see UIRESUME)
% uiwait(handles.figure1);

%answer = inputdlg('How Many Parameters?')
%setappdata(handles.axes1,'Parameters',answer)
% --- Outputs from this function are returned to the command line.
function varargout = THEREALGUI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes during object creation, after setting all properties.
function axes1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to axes1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: place code in OpeningFcn to populate axes1

% --- Executes when entered data in editable cell(s) in uitable1.
function uitable1_CellEditCallback(hObject, eventdata, handles)
% hObject    handle to uitable1 (see GCBO)
% eventdata  structure with the following fields (see MATLAB.UI.CONTROL.TABLE)
%	Indices: row and column indices of the cell(s) edited
%	PreviousData: previous data for the cell(s) edited
%	EditData: string(s) entered by the user
%	NewData: EditData or its converted form set on the Data property. Empty if Data was not changed
%	Error: error string when failed to convert EditData to appropriate value for Data
% handles    structure with handles and user data (see GUIDATA)

data=get(hObject,'data')
setappdata(handles.axes1, 'dataset', data)

% x=getappdata(handles.axes1,'Parameters')
% xx=str2double(x)
% a=repmat({'0'},xx,2)
% b=num2cell(a)
% set(handles.uitable1, 'data', a)
% set(handles.uitable1, 'ColumnEditable',true)

% --- Executes on button press in Graphpushbutton1.
function Graphpushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to Graphpushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

dataa= getappdata(handles.axes1, 'dataset')
x=dataa(1:end,1)
y=dataa(1:end,2)
xaxis=cell2mat(x)
yaxis=cell2mat(y)
plot(xaxis,yaxis)


% --- Executes on button press in Testing.
function Testing_Callback(hObject, eventdata, handles)
% hObject    handle to Testing (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% 
x=getappdata(handles.axes1,'Parameters')
xx=str2double(x)
a=repmat({'0'},xx,2)
b=num2cell(a)
set(handles.uitable1, 'data', a)
set(handles.uitable1, 'ColumnEditable',true)



function InputForParameters_Callback(hObject, eventdata, handles)
% hObject    handle to InputForParameters (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of InputForParameters as text
%        str2double(get(hObject,'String')) returns contents of InputForParameters as a double

para=get(handles.InputForParameters,'String')
setappdata(handles.axes1,'Parameters',para)

% --- Executes during object creation, after setting all properties.
function InputForParameters_CreateFcn(hObject, eventdata, handles)
% hObject    handle to InputForParameters (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in CreateTablePushButton.
function CreateTablePushButton_Callback(hObject, eventdata, handles)
% hObject    handle to CreateTablePushButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
x=getappdata(handles.axes1,'Parameters')
xx=str2double(x)
a=repmat({'0'},xx,2)
b=num2cell(a)
set(handles.uitable1, 'data', a)
set(handles.uitable1, 'ColumnEditable',true)